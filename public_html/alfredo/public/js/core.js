$(document).ready(function() {
    function body(tablaid) {
        html = "<tbody id =\"" + tablaid + "\"></tbody>"
        return html
    }

    function crearTablas(identificador, color, tablaid) {
        var tabla = $("#tablita")

        $("<table/>", {
            // PROPERTIES HERE
            id: identificador,
            "class": color, // ('class' is still better in quotes)
            append: "<thead><tr><th scope=\"col text-uppercase\">Descripció</th><th scope=\"col text-uppercase\">quantitat</th><th scope=\"col text-uppercase\">Preu</th><th scope=\"col text-uppercase\">Import</th></tr></thead>" + body(tablaid),
            appendTo: tabla // Finally, append to any selector

        })

    }
    crearTablas("true", "table table-striped bg-dark", "insert")
    crearTablas("false", "table table-striped bg-white", "insert-T")
        // Array de don alfredo
    carreto = [
        ["Asus VivoPC VM42-S255Z", 2, 345.59, false],
        ["HTC Vive Deluxe Audio Strap", 3, 119.50, true],
        ["Office 365", 10, 69.95, false],
        ["Windows Server 2012 R2", 2, 1582.6, true],
        ["Lenovo Legion Y520", 3, 799, false],
        ["Gafas xavier", 4, 100, true],
        ["camisateas Jordi", 6, 300, true]
    ]


    $('.btn-sm').css('margin', '5px')
    console.log("It works")

    $('.btn-danger').click(function(e) {
        alert("El color rojo es del peligro")
    })
    $('.btn-success').click(function(e) {
        alert("El color verde es de la vida")
    })
    $('.btn-info').click(function(e) {
        alert("El color celeste es para informar")
    })
    $('.btn-primary').click(function(e) {
        alert("El color azul es el generico")
    })

    $("#envia").click(function(e) {
        var content = $("#text").val()
        var color = $("#color").val()
        console.log(content + "-" + color)

        $("#botones").append(content).css("color", color)
    })
    $("#enviaConsola").click(function(e) {
        var content = $("#console").val()
        console.log(content)
    })

    var elemento = $("#insert")
    var elemento2 = $("#insert-T")
    var verdadero = 0
    var total = 0
    carreto.forEach(function(element) {
        var i = 0
        html = "<tr>"
        var importe = 1

        element.forEach(function(content) {

            if (i == 0 && typeof content == 'string') {
                html += "  <th scope=\"row\">" + content + "</th>"
            }
            if (i % 2 == 0 && typeof content == 'number') {

                html += "  <th>" + content + "</th>"
                importe *= content
            } else if (i % 2 != 0 && typeof content == 'number') {
                html += "  <th>" + content + "</th>"
                importe *= content
            }

            i++
            if (i == element.length) {

                if (!content) {
                    html += "<th>" + importe + "</th>"
                    html += "</tr>"

                    $(elemento2).append(html);
                    html = ""
                    total += importe

                } else {
                    html += "<th>" + importe + "</th>"
                    html += "</tr>"
                    console.log(html)
                    $(elemento).append(html);
                    html = ""
                    verdadero += importe
                }

            }
        })
    })

    $(elemento).append("<tr><th row=\"row\"><th>Import total</th><th>" + total + "€</th></th></tr><h1 class= \"text-center\">Nombres con false.</h1>");
    $(elemento2).append("<tr><th row=\"row\"><th>Import total</th><th>" + verdadero + "€</th></th></tr><h1 class= \"text-center\">Nombres con true.</h1>");


})